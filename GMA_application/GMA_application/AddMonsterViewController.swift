//
//  AddMonsterViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 4/28/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import UIKit
import CoreData

class AddMonsterViewController: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    var monster: Monster?
    var selectedItems = [String]()
    
    @IBOutlet weak var monsterInput: UITextField!
    
    @IBOutlet weak var cancel_btn: UIButton!
    @IBOutlet weak var done_btn: UIButton!
    
    @IBAction func add2combat(_ sender: UIButton) {
        self.performSegue(withIdentifier: "monster2combat", sender: self)
    }
    
    @IBAction func done(_ sender: UIButton) {
        guard let name = monsterInput.text, !name.isEmpty else {
            return //Add notice user cannot save empty items
        }
        
        if let monster = self.monster {
            monster.name = name
        } else {
            //Set values from input to the cell
            let monster = Monster(context: managedContext)
            monster.name = name
        }
        //Converting numbers: monster.level = Init16(levelInput.selectedTextFieldIndex) <-- not entirely sure, it was actually (segmentedControl.selectedSegmentIndex)
        
        do {
            try managedContext.save()
            monsterInput.resignFirstResponder()
            dismiss(animated: true)
        } catch {
            print("Error Saving Monster: \(error)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "monster2combat" {
            Combat.shared.allSelectedItems.append((monster?.name)!)
            let otherVc = segue.destination as! CombatSceneViewController
            print(Combat.shared.allSelectedItems)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        monsterInput.resignFirstResponder()
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        monsterInput.becomeFirstResponder()
        
        if let monster = monster {
            monsterInput.text = monster.name
            monsterInput.text = monster.name
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

