//
//  AddPlayerViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 4/28/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import UIKit
import CoreData

class AddPlayerViewController: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    var player: Player?
    var selectedItems = [String]()

    @IBOutlet weak var done_btn: UIButton!
    
    @IBOutlet weak var playerInput: UITextField!
    
    @IBOutlet weak var cancel_btn: UIButton!
    
    @IBAction func add2combat(_ sender: UIButton) {
        self.performSegue(withIdentifier: "player2combat", sender: self)
    }
    
    @IBAction func done(_ sender: UIButton) {
        guard let name = playerInput.text, !name.isEmpty else {
            return //Add notice user cannot save empty items
        }
        
        if let player = self.player {
            player.name = name
        } else {
            //Set values from input to the cell
            let player = Player(context: managedContext)
            player.name = name
        }
        
        do {
            try managedContext.save()
            playerInput.resignFirstResponder()
            dismiss(animated: true)
        } catch {
            print("Error Saving Player Character: \(error)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "player2combat" {
            Combat.shared.allSelectedItems.append((player?.name)!)
            let otherVc = segue.destination as! CombatSceneViewController
            print(Combat.shared.allSelectedItems)
        }
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        playerInput.resignFirstResponder()
        dismiss(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerInput.becomeFirstResponder()
        
        if let player = player {
            playerInput.text = player.name
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

