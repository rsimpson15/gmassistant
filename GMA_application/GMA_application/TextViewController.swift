//
//  TextViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 3/9/18.
//  Copyright © 2018 Rochelle Simpson. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {
    
    static func storyboardInstance() -> TextViewController? {
        let storyboard = UIStoryboard(name: "TextViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as? TextViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func back_btn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
