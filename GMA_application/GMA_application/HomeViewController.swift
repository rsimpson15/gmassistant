//
//  HomeViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 4/28/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    
    @IBAction func monsterBtn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "home2monster", sender: self)
    }
    
    let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn");
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //If the user isn't logged in, throw them off the page
    override func viewDidAppear(_ animated: Bool) {
        if(!isLoggedIn) {
            self.dismiss(animated: true, completion: nil);
        }
    }

}
