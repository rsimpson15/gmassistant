//
//  MonsterViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 4/28/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import UIKit
import CoreData

class MonsterViewController: UITableViewController {

    var resultsController: NSFetchedResultsController<Monster>!
    let CDS = coreDataStack()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request: NSFetchRequest<Monster> = Monster.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "level", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        resultsController = NSFetchedResultsController(
            fetchRequest: request,
            managedObjectContext: CDS.managedContext,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        resultsController.delegate = self
        
        do{
            try resultsController.performFetch()
        } catch {
            print("Perform Fetch Error: \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return resultsController.sections?[section].numberOfObjects ?? 0
    }

    override func tableView(_ _tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MonsterCell", for: indexPath)
        
        let monster = resultsController.object(at: indexPath)
        cell.textLabel?.text = monster.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "monster2form", sender: tableView.cellForRow(at: indexPath))
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style:.destructive, title: "Delete"){(action, view, completion) in
            let monster = self.resultsController.object(at: indexPath)
            self.resultsController.managedObjectContext.delete(monster)
            do {
                try self.resultsController.managedObjectContext.save()
                self.errorMessage(message: "Monster Deleted.");
                completion(true)
            } catch {
                print("Delete Failed: \(error)")
                self.errorMessage(message: "Failed to Delete Monster.");
                completion(false)
            }
        }
        action.image = UIImage(named: "trash")
        action.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style:.destructive, title: "Check"){(action, view, completion) in
            //Select Monster for Combat
            completion(true)
        }
        action.image = UIImage(named: "check")
        action.backgroundColor = .green
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let _ = sender as? UIBarButtonItem, let vc = segue.destination as? AddMonsterViewController{
            vc.managedContext = resultsController.managedObjectContext
        }
        
        if let cell = sender as? UITableViewCell, let vc = segue.destination as? AddMonsterViewController{
            vc.managedContext = resultsController.managedObjectContext
            if let indexPath = tableView.indexPath(for: cell){
                let monster = resultsController.object(at: indexPath)
                vc.monster = monster
            }
        }
    }
    
    //Error Function
    func errorMessage(message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle:UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        alert.addAction(okAction);
        self.present(alert, animated: true, completion:nil);
    }
}

extension MonsterViewController: NSFetchedResultsControllerDelegate{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type{
        case .insert:
            if let indexPath = newIndexPath{
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath{
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath){
                let monster = resultsController.object(at: indexPath)
                cell.textLabel?.text = monster.name
            }
        default:
            break
        }
    }
}
