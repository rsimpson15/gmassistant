//
//  GMA_application
//
//  Created by Rochelle Simpson on 5/26/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import Foundation

class Combat {
    
    static let shared = Combat()
    
    var allPlayers = [Player]()
    
    var allMonsters = [Monster]()
    
    var allNPCs = [Nonplayer]()
    
    var allSelectedItems = [String]()
    
}
