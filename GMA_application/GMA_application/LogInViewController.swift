//
//  LogInViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 3/4/18.
//  Copyright © 2018 Rochelle Simpson. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {
    
    @IBOutlet weak var userName_text: UITextField!
    @IBOutlet weak var userPassword_text: UITextField!

    @IBAction func back_btn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reg_btn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "logIn2reg", sender: self)
    }
    
    @IBAction func submit_btn(_ sender: UIButton) {
        let userName = userName_text.text;
        let userPassword = userPassword_text.text;
        
        //Reads from Users Defaults
        let storedPassword = UserDefaults.standard.string(forKey: "userPassword");
        let storedName = UserDefaults.standard.string(forKey: "userName");
        
        if (userName == storedName){
            if (userPassword == storedPassword){
                //Successful Log In Acheived
                UserDefaults.standard.set(true, forKey: "isLoggedIn");
                UserDefaults.standard.synchronize();
                self.performSegue(withIdentifier: "logIn2home", sender: self)
            } else {
                errorMessage(message: "Incorrect Password. Please Try Again.")
            }
        } else {
            errorMessage(message: "Incorrect User Name. Please Try Again.")
        }
    }
    
    //Error Function
    func errorMessage(message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle:UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        alert.addAction(okAction);
        self.present(alert, animated: true, completion:nil);
    }
    
    static func storyboardInstance() -> LogInViewController? {
        let storyboard = UIStoryboard(name: "LogInViewConroller", bundle: nil)
        return storyboard.instantiateInitialViewController() as? LogInViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}
