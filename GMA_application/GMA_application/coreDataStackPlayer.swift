//
//  coreDataStackPlayer.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 5/7/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import Foundation
import CoreData

class coreDataStackPlayer {
    var container: NSPersistentContainer{
        let container = NSPersistentContainer(name: "Players")
        container.loadPersistentStores{ (description, error) in
            guard error == nil else {
                print("Error: \(error!)")
                return
            }
        }
        return container
    }
    
    var managedContext: NSManagedObjectContext{
        return container.viewContext
    }
}
