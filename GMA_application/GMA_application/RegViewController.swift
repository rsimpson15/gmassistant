//
//  RegViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 3/4/18.
//  Copyright © 2018 Rochelle Simpson. All rights reserved.
//

import UIKit

class RegViewController: UIViewController {
    
    @IBOutlet weak var userName_text: UITextField!
    @IBOutlet weak var userEmail_text: UITextField!
    @IBOutlet weak var userPassword_try1: UITextField!
    @IBOutlet weak var userPassword_try2: UITextField!
    
    @IBAction func back_btn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func cancel_btn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func submit_btn(_ sender: UIButton) {
        let userEmail = userEmail_text.text;
        let userName = userName_text.text;
        let userPassword1 = userPassword_try1.text;
        let userPassword2 = userPassword_try2.text;
        
        //Prevent Empty Entries to Form
        if((userEmail?.isEmpty)! || (userName?.isEmpty)! || (userPassword1?.isEmpty)! || (userPassword2?.isEmpty)!){
            //Throw Error to User
            errorMessage(message: "Please fill in all fields.");
            return;
        }
        
        //Validate Password Field
        if(userPassword1 != userPassword2){
            //Throw Error to User
            errorMessage(message: "Passwords do not match.");
            return;
        }
        
        //Store Data
        //Will be revamped with database logic in a later task/PBI but currently stores data locally
        UserDefaults.standard.set(userName, forKey: "userName");
        UserDefaults.standard.set(userEmail, forKey: "userEmail");
        UserDefaults.standard.set(userPassword1, forKey: "userPassword");
        UserDefaults.standard.synchronize();
        
        //Confirmation Message for User
        let alert = UIAlertController(title: "Alert", message: "Registration Complete", preferredStyle:UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default){action in
            self.performSegue(withIdentifier: "reg2logIn", sender: self)
        }
        alert.addAction(okAction);
        //PresentViewController?
        self.present(alert, animated: true, completion:nil);
    }
    
    //Error Function
    func errorMessage(message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle:UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        alert.addAction(okAction);
        self.present(alert, animated: true, completion:nil);
    }

    static func storyboardInstance() -> RegViewController? {
        let storyboard = UIStoryboard(name: "RegViewController", bundle: nil)
        return storyboard.instantiateInitialViewController() as? RegViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}
