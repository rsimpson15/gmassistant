//
//  CombatSceneViewController.swift
//  GMA_application
//
//  Created by Rochelle Simpson on 5/22/18.
//  Copyright © 2018 RochelleSimpson. All rights reserved.
//

import UIKit
import CoreData

class CombatSceneViewController: UITableViewController{
    
    var selectedItems : [String] = []
    var shuffled = [String]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let otherVC = AddPlayerViewController()
        selectedItems = Combat.shared.allSelectedItems
        
        for i in 0..<selectedItems.count
        {
            let rand = Int(arc4random_uniform(UInt32(selectedItems.count)))
            
            shuffled.append(selectedItems[rand])
            
            selectedItems.remove(at: rand)
        }
        
        print(selectedItems)
        print(shuffled)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shuffled.count
    }
    
    override func tableView(_ _tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CombatCell", for: indexPath as IndexPath)
        cell.textLabel?.text = shuffled[indexPath.item]
        return cell
    }
    
    //Error Function
    func errorMessage(message:String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle:UIAlertControllerStyle.alert);
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        alert.addAction(okAction);
        self.present(alert, animated: true, completion:nil);
    }
    

}
